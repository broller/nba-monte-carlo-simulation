import json
import statistics
from scipy.stats import norm
import random
import pandas as pd

def openJSONFile():
    with open('./data_files/nba_per_game_stats.json') as json_file:
        data = json.load(json_file)
        return data

def getPerGameTeamStats(team, data):
    headers = ['3PM', '3PA']
    
    teamStats = {'gp': 0, '3PM': 0, '3PA': 0}
    threePM = []
    for i in range(len(data)):
        if data[i]['awayTeam'] == team:
            teamStats['3PM'] += int(data[i]['away3PM'])
            teamStats['3PA'] += int(data[i]['away3PA'])
            teamStats['gp'] += 1
            threePM.append(int(data[i]['away3PM']))
        elif data[i]['homeTeam'] == team:
            teamStats['3PM'] += int(data[i]['home3PM'])
            teamStats['3PA'] += int(data[i]['home3PA'])
            teamStats['gp'] += 1
            threePM.append(int(data[i]['home3PM']))


    teamStats['3P%'] = (teamStats['3PM'] / teamStats['3PA']) * 100
    teamStats['3PM/G'] = statistics.mean(threePM)
    teamStats['STD'] = statistics.stdev(threePM)
    
    return teamStats

def getTeamsStats(table, team):
    teamStats = {}
    teamStats['3PA'] = perGameStats.loc[perGameStats['awayTeam'] == team, 'away3PA'].sum()  + perGameStats.loc[perGameStats['homeTeam'] == team, 'home3PA'].sum()
    teamStats['3PM'] = perGameStats.loc[perGameStats['awayTeam'] == team, 'away3P'].sum() + perGameStats.loc[perGameStats['homeTeam'] == team, 'home3P'].sum()
    teamStats['gp'] = perGameStats.loc[perGameStats['awayTeam'] == team, 'away3P'].count() + perGameStats.loc[perGameStats['homeTeam'] == team, 'home3P'].count()
    teamStats['3P%'] = (teamStats['3PM'] / teamStats['3PA']) * 100
    
    threePM = perGameStats.loc[perGameStats['awayTeam'] == team, 'away3P'].tolist() + perGameStats.loc[perGameStats['homeTeam'] == team, 'home3P'].tolist()
    print(threePM)
    teamStats['3PM/G'] = statistics.mean(threePM)
    teamStats['STD'] = statistics.stdev(threePM)
    teamStats['3P%'] = (teamStats['3PM'] / teamStats['3PA']) * 100
    
    return teamStats

perGameStats = pd.read_json('./data_files/nba_per_game_stats.json')
# perGameStats = openJSONFile()

# table = pd.DataFrame.from_dict(perGameStats, orient='index')
# team1Stats = getPerGameTeamStats('GSW', perGameStats)
# team2Stats = getPerGameTeamStats('LAC', perGameStats)
team1Stats = getTeamsStats(perGameStats, "LAL")
team2Stats = getTeamsStats(perGameStats, 'CHO')

team1Wins = 0
team2Wins = 0
for number in range(0, 100000):
    team1Dist = norm.ppf(random.uniform(0, 1), loc=team1Stats['3PM/G'], scale=team1Stats['STD'])
    team2Dist = norm.ppf(random.uniform(0, 1), loc=team2Stats['3PM/G'], scale=team2Stats['STD'])
    if team1Dist > team2Dist:
        team1Wins += 1
    else:
        team2Wins +=1

print('team1 wins ' + str(( (team1Wins / 100000) * 100) ) + ' percent of the time')
print('team2 wins ' + str(( (team2Wins / 100000) * 100) ) + ' percent of the time')