from bs4 import BeautifulSoup
import requests
import pandas as pd
import json

baseURL = 'https://www.basketball-reference.com'

def writeFile(data, fileName, dataType):
    file = open(fileName, dataType)
    file.write(data)
    file.close()

def writeJSON(data):
    with open('data/nba_per_game_stats.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

def getTeamIdentifier(text):
    return text.split(".")[0]

def getPlayedGames(year, month):
    # URL page we will scraping (see image above)
    url = baseURL + "/leagues/NBA_{year}_games-{month}.html".format(year=year, month=month)
    response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    # this is the HTML from the given URL
    # html = urlopen(url)
    html = response.content
    soup = BeautifulSoup(html, features="html.parser")

    table = soup.find("table", {"id": "schedule"})

    # headers = [th.getText() for th in table.findAll('tr', limit=2)[0].findAll('th')]
    # headers = headers[1:]

    rows = table.findAll('tr')[1:]
    stats = []
    for i in range(len(rows)):
        if rows[i].find("td", {"data-stat":"box_score_text"}).find("a") and rows[i].find("td", {"data-stat":"box_score_text"}).find("a")['href']:
            awayTeam = getTeamIdentifier(rows[i].find("td", {"data-stat":"visitor_team_name"})['csk'])
            homeTeam = getTeamIdentifier(rows[i].find("td", {"data-stat":"home_team_name"})['csk'])
            link = rows[i].find("td", {"data-stat":"box_score_text"}).find("a")['href']
            stats.append({ 'homeTeam': homeTeam, 'awayTeam': awayTeam, 'link': link})

    return stats

def getStats(game):
    url = baseURL + game['link']
    response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    # this is the HTML from the given URL
    # html = urlopen(url)
    html = response.content
    soup = BeautifulSoup(html, features="html.parser")

    awayStats = soup.find("table", {"id": "box-" + game['awayTeam'] + "-game-basic"})
    homeStats = soup.find("table", {"id": "box-" + game['homeTeam'] + "-game-basic"})
    
    # get headers for each team
    awayHeaders = [('away' + th.getText()) for th in awayStats.findAll('tr', limit=2)[1].findAll('th')]
    awayHeaders = awayHeaders[1:]

    homeHeaders = [('home' + th.getText()) for th in homeStats.findAll('tr', limit=2)[1].findAll('th')]
    homeHeaders = homeHeaders[1:]

    # insert team name and combine
    awayHeaders.insert(0,'awayTeam')
    homeHeaders.insert(0, 'homeTeam')
    gameHeaders = awayHeaders + homeHeaders

    # get away stats and add team name
    rows = awayStats.findAll('tr')[2:]
    awayTeamStats = rows[-1]
    away_stats_row = [td.getText() for td in awayTeamStats.findAll('td')]
    away_stats_row.insert(0, game['awayTeam'])

    # get home stats and combine
    rows = homeStats.findAll('tr')[2:]
    homeTeamStats =rows[-1]
    home_stats_row = [td.getText() for td in homeTeamStats.findAll('td')]
    home_stats_row.insert(0, game['homeTeam'])
    gameStats = [away_stats_row + home_stats_row]

    # awayTable = pd.DataFrame(away_stats_row, columns = awayHeaders)
    # homeTable = pd.DataFrame(home_stats_row, columns = homeHeaders)
    gameTable = pd.DataFrame(gameStats, columns = gameHeaders)

    return gameTable

# 'december', 'january', 'february', 'march', 'april', 'may'
months = ['october', 'november', 'december', 'january', 'february']

gamesPlayed = []
for i in range(len(months)):
    gamesPlayed += getPlayedGames(2022, months[i])

games = None
for i in range(len(gamesPlayed)):
    gameData = getStats(gamesPlayed[i])
    if i == 0:
        games = gameData
    else:
        games.loc[i] = gameData.loc[0]


games.to_json(r'data/nba_per_game_stats.json')


# =IF(oddsOffered>0,(% team won in Monte Carlo SIM*(oddsOffered+100)-100)/100,(% team won in Monte Carlo SIM * (ABS(oddsOffered)+100)-ABS(oddsOffered))/ABS(oddsOffered)
# if oddsOffered > 0
    # (% team won in Monte Carlo SIM*(oddsOffered+100)-100)/100
# else
    # (% team won in Monte Carlo SIM * (ABS(oddsOffered)+100)-ABS(oddsOffered))/ABS(oddsOffered)
