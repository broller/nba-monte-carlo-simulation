import json
import statistics
from scipy.stats import norm
import random
import pandas as pd
from pathlib import Path

class LeaguePerGameStatsTable:
    def __init__(self):
        path = Path(__file__).parent.absolute() / 'data' / 'nba_per_game_stats.json'
        self.table = pd.read_json(path)

    def getTeamsStats(self, team):
        teamStats = {}
        teamStats['3PA'] = self.table.loc[self.table['awayTeam'] == team, 'away3PA'].sum()  + self.table.loc[self.table['homeTeam'] == team, 'home3PA'].sum()
        teamStats['3PM'] = self.table.loc[self.table['awayTeam'] == team, 'away3P'].sum() + self.table.loc[self.table['homeTeam'] == team, 'home3P'].sum()
        teamStats['gp'] = self.table.loc[self.table['awayTeam'] == team, 'away3P'].count() + self.table.loc[self.table['homeTeam'] == team, 'home3P'].count()
        teamStats['3P%'] = (teamStats['3PM'] / teamStats['3PA']) * 100
        
        threePM = self.table.loc[self.table['awayTeam'] == team, 'away3P'].tolist() + self.table.loc[self.table['homeTeam'] == team, 'home3P'].tolist()
        teamStats['3PM/G'] = statistics.mean(threePM)
        teamStats['STD'] = statistics.stdev(threePM)
        teamStats['3P%'] = (teamStats['3PM'] / teamStats['3PA']) * 100
        
        return teamStats

    def getTeamStatsByField(self, team, statsFields):
        teamStats = []
        for stat in statsFields:
            awayStats = self.table.loc[self.table['awayTeam'] == team]['away' + stat]
            homeStats = self.table.loc[self.table['homeTeam'] == team]['home' + stat]
            combinedSeries = awayStats.append(homeStats, ignore_index=True)
            df = pd.DataFrame(combinedSeries)
            df = df.rename(columns = {0:stat})
            teamStats.append(df)

        return pd.concat(teamStats, axis=1)

    def sum(self, teamTable, field):
        return teamTable[field].sum()

    def count(self, teamTable, field):
        return teamTable[field].count()
    

# table = LeaguePerGameStatsTable()
# # team1 = table.getTeamsStats('LAC')
# team2 = table.getTeamStatsByField('LAC', ['3PA', '3P'])
# monteCarloObj = {}
# monteCarloObj['3PA'] = table.sum(team2, '3PA')
# monteCarloObj['3PM'] = table.sum(team2, '3P')
# monteCarloObj['gp'] = table.count(team2, '3P')
# monteCarloObj['3P%'] = (monteCarloObj['3PM'] / monteCarloObj['3PA']) * 100
# monteCarloObj['3PM/G'] = team2['3P'].mean()
# monteCarloObj['STD'] = team2['3P'].std()
