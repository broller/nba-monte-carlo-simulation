import pandas as pd
from pathlib import Path

class LeaguePacePerGameTable:
    def __init__(self):
        path = Path(__file__).parent.absolute() / 'data' / 'nba_pace_per_game_stats.json'
        self.table = pd.read_json(path)
    
    def getTeamStatsByField(self, team, statsFields):
        teamStats = []
        for stat in statsFields:
            awayStats = self.table.loc[self.table['awayTeam'] == team]['away' + stat]
            homeStats = self.table.loc[self.table['homeTeam'] == team]['home' + stat]
            combinedSeries = awayStats.append(homeStats, ignore_index=True)
            df = pd.DataFrame(combinedSeries)
            df = df.rename(columns = {0:stat})
            teamStats.append(df)

        return pd.concat(teamStats, axis=1)