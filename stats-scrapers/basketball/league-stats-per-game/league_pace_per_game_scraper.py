from bs4 import BeautifulSoup
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd

baseURL = 'https://www.basketball-reference.com'
months = ['october', 'november', 'december', 'january', 'february']

def getTeamIdentifier(text):
    return text.split(".")[0]

def getPlayedGames(year, month):
    # URL page we will scraping (see image above)
    url = baseURL + "/leagues/NBA_{year}_games-{month}.html".format(year=year, month=month)
    response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    html = response.content
    soup = BeautifulSoup(html, features="html.parser")

    table = soup.find("table", {"id": "schedule"})
    rows = table.findAll('tr')[1:]
    stats = []
    for i in range(len(rows)):
        if rows[i].find("td", {"data-stat":"box_score_text"}).find("a") and rows[i].find("td", {"data-stat":"box_score_text"}).find("a")['href']:
            awayTeam = getTeamIdentifier(rows[i].find("td", {"data-stat":"visitor_team_name"})['csk'])
            homeTeam = getTeamIdentifier(rows[i].find("td", {"data-stat":"home_team_name"})['csk'])
            link = rows[i].find("td", {"data-stat":"box_score_text"}).find("a")['href']
            stats.append({ 'homeTeam': homeTeam, 'awayTeam': awayTeam, 'link': link})

    return stats

def getFourFactors(browser, game):
    url = baseURL + game['link']
    browser.get(url)
    timeout = 60
    try:
        element_present = EC.presence_of_element_located((By.ID, 'four_factors'))
        WebDriverWait(browser, timeout).until(element_present)
    except TimeoutException:
        print("Timed out waiting for page to load")
        return None

    html = browser.page_source
    soup = BeautifulSoup(html, features="html.parser")
    scores = getScore(soup)
    fourFactors = soup.find("table", {"id": "four_factors"})
    headers = [(th.getText()) for th in fourFactors.find('thead').findAll('th')][4:] 
    headers = ['Team', 'Score'] + headers
    homeHeaders = ['home' + header for header in headers]
    awayHeaders = ['away' + header for header in headers]

    rows = fourFactors.find('tbody').find_all('tr')
    teamNames = [th.getText() for th in fourFactors.find('tbody').find_all('th')]
    team1 = [td.getText() for td in rows[0].findAll('td')]
    team1 = [teamNames[0], scores[0]] + team1
    team2 = [td.getText() for td in rows[1].findAll('td')]
    team2 = [teamNames[1], scores[1]] + team2
    gameTable = pd.DataFrame([team1 + team2], columns = awayHeaders + homeHeaders)
    return gameTable

def getScore(soup):
    scoreTable = [table for table in soup.findAll('table') if table.get('id') == 'line_score'][0].find('tbody').find_all('tr')
    scores = [scoreTable[0].findAll('td')[-1].getText(), scoreTable[1].findAll('td')[-1].getText()]
    return scores

gamesPlayed = []
for i in range(len(months)):
    gamesPlayed += getPlayedGames(2022, months[i])

options = webdriver.ChromeOptions()
options.add_argument('headless')
browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
games = None
for i in range(len(gamesPlayed)):
    print( str(i) + ' of ' + str(len(gamesPlayed)) )
    gameData = getFourFactors(browser, gamesPlayed[i])
    if i == 0:
        games = gameData
    else:
        games.loc[i] = gameData.loc[0]
browser.quit()

games.to_json(r'data/nba_pace_per_game_stats.json')