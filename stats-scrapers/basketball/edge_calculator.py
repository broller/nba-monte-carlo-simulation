import sys

class EdgeCalculator:
    def __init__(self):
        super().__init__()

    def calculateEdge(self, odds, winPct):
        # print('odds are ' + str(odds))
        # print('win % is ' + str(winPct))
        if(odds > 0):
            edge =  (winPct * (odds + 100) - 100) / 100
        else:
            edge = (winPct *  (abs(odds) + 100) - abs(odds))  / abs(odds)
        
        return edge

    def americanToDecimal(self, odds):
        if odds < 0:
            return round(1 - (100 / odds), 2)
        else:
            return round(1 + (odds / 100), 2) 

# =IF(oddsOffered>0, (% team won in Monte Carlo SIM*(oddsOffered+100)-100)/100,(% team won in Monte Carlo SIM * (ABS(oddsOffered)+100)-ABS(oddsOffered))/ABS(oddsOffered)
# if oddsOffered > 0
    # (% team won in Monte Carlo SIM*(oddsOffered+100)-100)/100
# else
    # (% team won in Monte Carlo SIM * (ABS(oddsOffered)+100)-ABS(oddsOffered))/ABS(oddsOffered)
# if len(sys.argv) > 2:
#     odds = float(sys.argv[1])
#     winpct = float(sys.argv[2])
#     if(odds > 0):
#         edge =  (winpct * (odds + 100) - 100) / 100
#         print(edge)
#     else:
#         edge = (winpct *  (abs(odds) + 100) - abs(odds))  / abs(odds)
#         print(edge)
# else:
#     print('Error: odds or win percentage not supplied')