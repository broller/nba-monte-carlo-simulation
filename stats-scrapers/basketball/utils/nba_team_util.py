import json
from pathlib import Path

class NBATeamUtil:
    def __init__(self):
        path = Path(__file__).parent.absolute() / 'teams.json'
        with open(path) as json_file:
            self.teams = json.load(json_file)

    def getKeys(self):
        return self.teams.keys()
    
    def getTeam(self, key):
        if not self.teams.get(key):
            print('Error: this team does not exist')

        return self.teams.get(key)

    def getKey(self, team):
        for id, displayName in self.teams.items():
            if displayName == team:
                return id
        return None