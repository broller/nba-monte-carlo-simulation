from bs4 import BeautifulSoup
import requests
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from webdriver_manager.chrome import ChromeDriverManager

def writeFile(data, fileName, dataType):
    file = open(fileName, dataType)
    file.write(data)
    file.close()

def getLeagueSummaryTables(year):
    url = 'https://www.basketball-reference.com/leagues/NBA_{year}.html'.format(year=year)
    # for reading dynamic content, but having to wait longer
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    browser = webdriver.Chrome(ChromeDriverManager().install())
    browser.get(url)
    timeout = 30
    try:
        element_present = EC.presence_of_element_located((By.ID, 'div_per_game-team'))
        WebDriverWait(browser, timeout).until(element_present)
    except TimeoutException:
        print("Timed out waiting for page to load")
        return None

    html = browser.page_source
    soup = BeautifulSoup(html, features="html.parser")

    teamTable = soup.find("table", {"id": "totals-team"})
    oppTable = soup.find("table", {"id": "totals-opponent"})

    headers = [th.getText() for th in teamTable.findAll('tr', limit=2)[0].findAll('th')]
    headers = headers[1:]

    rows = teamTable.findAll('tr')[1:]
    team_stats = [[td.getText() for td in rows[i].findAll('td')] for i in range(len(rows))]
    team_stats.pop();
    teamStats = pd.DataFrame(team_stats, columns = headers).drop(columns=['MP'])

    oppRows = oppTable.findAll('tr')[1:]
    opp_stats = [[td.getText() for td in oppRows[i].findAll('td')] for i in range(len(oppRows))]
    opp_stats.pop();
    oppStats = pd.DataFrame(opp_stats, columns = headers).drop(columns=['MP'])

    leagueAverage = getLeagueAverageTable(soup)

    return [normalizeTeamNames(teamStats), normalizeTeamNames(oppStats), leagueAverage]

def getLeagueAverageTable(soup):
    leagueAverageTable = soup.find("table", {"id": "per_game-team"})
    
    headers = [th.getText() for th in leagueAverageTable.findAll('tr', limit=2)[0].findAll('th')]
    headers = headers[1:]

    rows = leagueAverageTable.findAll('tr')[1:]
    
    leagueAvgStats = [[td.getText() for td in rows[i].findAll('td')] for i in range(len(rows))]
    leagueAvgRow =  leagueAvgStats.pop();
    leagueAverage = pd.DataFrame([leagueAvgRow], columns=headers).drop(columns=['MP'])
    return leagueAverage

def normalizeTeamNames(data):
    data['Team'] = data['Team'].str.replace(r'\*', '')
    return data

tables = getLeagueSummaryTables(2022)

tables[0].to_json(r'data/teamStats.json')
tables[1].to_json(r'data/awayStats.json')
tables[2].to_json(r'data/leagueAvgStats.json')

# headers: Team  G    FG   FGA   FG%    3P   3PA   3P%    2P   2PA   2P%    FT   FTA   FT%   ORB   DRB   TRB   AST   STL  BLK   TOV    PF    PTS