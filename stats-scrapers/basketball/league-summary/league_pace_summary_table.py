import pandas as pd
from pathlib import Path

class LeaguePaceSummaryTable:
    def __init__(self):
        path = Path(__file__).parent.absolute() / 'data'
        self.table = pd.read_csv(path / 'league_pace_sumary.csv', header=None).drop([0,0])
        new_header = self.table.iloc[0] #grab the first row for the header
        self.table = self.table[1:] #take the data less the header row
        self.table.columns = new_header
        self.table = self.table.drop(['Age', 'Arena', 'Attend.', 'Attend./G'], axis=1)
    
    def getTeamPaceStats(self, teamID):
        return self.table.loc[self.table['Team'] == teamID][['Team', 'ORtg','DRtg', 'Pace']]

    def getLeagueAverage(self):
        return self.table[-1:][['ORtg','DRtg', 'Pace']]

    def getPace(self):
        return self.table['Pace']



# table = LeaguePaceSummaryTable()
# print(table.table)
# print(table.getTeamPaceStats('Milwaukee Bucks'))
# print(table.getLeagueAverage())