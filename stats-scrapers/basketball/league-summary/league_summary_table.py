import pandas as pd
from pathlib import Path
from nba_team_util import NBATeamUtil

class LeagueSummaryTable:
    def __init__(self):
        path = Path(__file__).parent.absolute() / 'data'
        self.homeTable = pd.read_json(path / 'teamStats.json')
        self.awayTable = pd.read_json(path / 'awayStats.json')
        self.leagueAvgTable = pd.read_json(path / 'leagueAvgStats.json')
        self.teamDict = NBATeamUtil()
    
    def getTeam(self, team, statsFields):
        headers = ['id']
        for stat in statsFields:
            if stat == 'G':
                headers.append(stat)
            else:
                headers.append(stat)
                headers.append('o' + stat)

        teamTable = pd.DataFrame(columns=headers)

        homeRow = self.homeTable.loc[self.homeTable['Team'] == self.teamDict.getTeam(team)]
        awayRow = self.awayTable.loc[self.awayTable['Team'] == self.teamDict.getTeam(team)]
        teamTable['id'] = [team]

        for stat in statsFields:
            if stat == 'G':
                teamTable[stat] = homeRow.iloc[0][stat]
            else:
                teamTable['o' + stat] = [ awayRow.iloc[0][stat] ]
                teamTable[stat] = [ homeRow.iloc[0][stat] ]
        
        return teamTable

    def addAvg(self, table, statsFields):
        for stat in statsFields:
            table[stat + 'Avg'] = table.iloc[0][stat] / table.iloc[0]['G']
            table['o' + stat + 'Avg'] = table.iloc[0]['o' + stat] / table.iloc[0]['G']

        return

    def getLeagueAvg(self, statsFields):
        teamTable = pd.DataFrame(columns=statsFields)
        for stat in statsFields:
            teamTable[stat] = [self.leagueAvgTable.iloc[0][stat]]

        return teamTable

# table = LeagueSummaryTable()
# team1 = table.getTeam('ORL', ['3P', '3PA', '3P%','G'])
# print(team1)
# table.addAvg(team1, ['3P', '3PA'])

# leageAVG = table.getLeagueAvg(['3P', '3PA', '3P%'])
# print(leageAVG)
# team2 = table.getTeam('Brooklyn Nets', ['3P', '3PA', '3P%','G'])

# print(team1)
# print(team2)