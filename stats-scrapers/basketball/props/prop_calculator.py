import pandas as pd
from scipy.stats import poisson
import sys
sys.path.append('../')
sys.path.append('/Users/brandonr/Software_Projects/wagering_tools/scrapers/stats-scrapers/basketball/')
from edge_calculator import EdgeCalculator

# 'Rk', 'Unnamed: 5', 'Unnamed: 7', 
unusedColumns = [ 'G', 'Age', 'TRB', 'AST', 'STL', 'ORB', 'DRB', 'BLK', 'TOV', 'GmSc']

class PropsCalculator:
    def __init__(self):
        super().__init__()
        self.edgeCalculator = EdgeCalculator()


    def removeNonPlayingGames(self, frame):
        return frame[(frame.GS != 'Did Not Dress') & (frame.GS != 'Inactive') & (frame.GS != 'Did Not Play') & (frame.GS != 'Not With Team') & (frame.GS != 'Player Suspended')]

    def getVariance(self, df):
        allValues = []
        for column in df:
            column_val = df[column].tolist()
            allValues += column_val

        one_column_df = pd.DataFrame(allValues)
        return one_column_df.var().iloc[0]

# if len(sys.argv) >= 5:
#     name = sys.argv[1]
#     points = float(sys.argv[2])
#     overOdds = int(sys.argv[3])
#     underOdds = int(sys.argv[4])

    def calculatePropValue(self, prop, gameLogs):
        gameLogs = self.removeNonPlayingGames(gameLogs).drop(unusedColumns, axis=1)
        bootstrap = pd.DataFrame()
        for x in range(0, 100):
            for y in range(0, 10):
                sample = gameLogs['PTS'].sample()
                bootstrap.loc[x, y] = int(sample.iloc[0])
        bootstrap['mean'] = bootstrap.iloc[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]].mean(axis=1)
        bootstrap['median'] = bootstrap.iloc[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]].median(axis=1)

        calculatedMean = bootstrap['mean'].mean()
        calculatedMedian = bootstrap['median'].median()
        variance = self.getVariance(bootstrap)

        value = float(prop['value'])
        over = 1 - poisson.cdf(value, calculatedMean)
        under = poisson.cdf(value, calculatedMean)

        overOdds = float(prop['overOdds'])
        underOdds = float(prop['underOdds'])
        prop['overEdge'] = self.edgeCalculator.calculateEdge(overOdds, over)
        prop['underEdge'] = self.edgeCalculator.calculateEdge(underOdds, under)

        # print( "{:.0%}".format(self.edgeCalculator.calculateEdge(overOdds, over)) )
        # print("{:.0%}".format(self.edgeCalculator.calculateEdge(underOdds, under)) )
        return prop