import os.path
from pathlib import Path
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup

class PBPropsScraper:
    def __init__(self):
        super().__init__()
    
    def writeProps(self, eventId):
        filename = str(Path(__file__).parent.absolute()) + '/temp/pb_{event}.html'.format(event=eventId)
        if not os.path.exists(filename):
            url = 'https://in.pointsbet.com/sports/basketball/NBA/{event}'.format(event=eventId)
            options = webdriver.ChromeOptions()
            # options.add_argument('headless')
            browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
            browser.get(url)
            timeout = 10000
            try:
                element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div[1]/div/div/div[4]/div[1]/div/div/div[2]/div[4]/div/div[2]/div/div[4]')) 
                WebDriverWait(browser, timeout).until(element_present)
                browser.implicitly_wait(10)
            except TimeoutException:
                print("Timed out waiting for page to load")
            
            buttons = browser.find_elements_by_tag_name('button')
            for button in buttons:
                if button.text == 'PLAYER POINTS':
                    button.click()
                    browser.implicitly_wait(10)

            html = browser.page_source
            self.writeFile(html, filename,"w")
            browser.quit()
            soup = BeautifulSoup(html, features="html.parser")
        else:
            soup = BeautifulSoup(open(filename), features="html.parser")
        
        market = self.findMarket(soup.findAll('div', {'class': 'f18dqkdv'}), 'Player Points')
        propsObj = self.buildMarketObject(market)
        return propsObj
    
    def buildMarketObject(self, players):
        markets = {}
        for i, player in enumerate(players):
            ouElements = player.findAll('button')
            over = ouElements[0].findAll('span')
            under = ouElements[1].findAll('span')
            parts = over[0].text.split()
            markets[i] = {"playerName": ' '.join(parts[:-2]), "value" : parts[len(parts) - 1], "overOdds": over[1].text, "underOdds": under[1].text}

        return markets

    def findMarket(self, markets, identifier):
        oddsButtons = []
        for market in markets:
            if market.find('button').find('span').text == identifier:
                wagerGroups = market.findAll('div', {'class': 'faxe22p'})
                for group in wagerGroups:
                    buttons = group.findAll('button')
                    if buttons[0].text.find('Over') != -1 or buttons[0].text.find('Under') != -1:
                        oddsButtons.append(group)
                break

        return oddsButtons

    def writeFile(self, data, fileName, dataType):
        file = open(fileName, dataType)
        file.write(data)
        file.close()

# scraper = PBPropsScraper()
# print(scraper.writeProps('240970'))