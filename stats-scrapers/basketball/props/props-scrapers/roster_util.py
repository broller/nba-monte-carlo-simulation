import requests
import unicodedata
import os.path
from pathlib import Path
from bs4 import BeautifulSoup, element
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

class RosterUtil:
    def __init__(self):
        super().__init__()

    def getRoster(self, teamId, year):
        fileName = str(Path(__file__).parent.absolute()) + '/temp/' + teamId + '.html'
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument("--log-level=3")
        browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        if os.path.exists(fileName):
            browser.get('file://' + fileName)
        else:
            url = 'https://www.basketball-reference.com/teams/{teamId}/{year}.html'.format(teamId=teamId, year=year)
            browser.get(url)
            timeout = 10000
            try:
                element_present = EC.presence_of_element_located((By.ID, 'roster')) 
                WebDriverWait(browser, timeout).until(element_present)
            except TimeoutException:
                print("Timed out waiting for page to load")
            self.writeFile(browser.page_source, fileName, 'w')
        
        trs = browser.find_element_by_id('roster').find_element_by_tag_name('tbody').find_elements_by_tag_name('tr')
        roster = []
        for tr in trs:
            entry = self.getRosterEntry(tr)
            roster.append(entry)
        
        return roster
    
    def getRosterEntry(self, els):
        player = els.find_element_by_xpath('td[@data-stat="player"]') #.findElements(By.cssSelector("*[data-stat='player']"))  #_ .find('td', {'data-stat', 'player'})
        id = player.find_element_by_tag_name('a').get_attribute('href').replace('https://www.basketball-reference.com/','').replace('.html', '').replace('file:///', '')
        return { "name": player.text, "id": id } 

    def findPlayer(self, list, playerName):
        for player in list:
            if self.normalized(self.strip_accents(player['name'])) == self.normalized(playerName):
                return player
        return None

    def normalized(self, name):
        return name.replace('-', ' ').replace('.', '')

    def strip_accents(self, s):
        return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')

    def writeFile(self, data, fileName, dataType):
        file = open(fileName, dataType)
        file.write(data)
        file.close()