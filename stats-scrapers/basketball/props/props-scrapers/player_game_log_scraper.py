from bs4 import BeautifulSoup
import os.path
from pathlib import Path
import requests
import pandas as pd

class PlayerGameLogScraper:
    def __init__(self):
        super().__init__()

    def getPlayerGameLog(self, id, year):
        fileName = str(Path(__file__).parent.absolute()) + '/temp/' + id.split('/')[2] + '.html'
        if os.path.exists(fileName):
            soup = BeautifulSoup(open(fileName), features="html.parser")
        else:
            url = "https://www.basketball-reference.com/{id}/gamelog/{year}".format(id=id, year=year)
            response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
            html = response.content
            self.writeFile(html, fileName, 'wb')
            soup = BeautifulSoup(html, features="html.parser")
        table = soup.find('table', {"id": "pgl_basic"})
        headers = self.get_table_headers(table)
        rows = self.get_table_rows(table)
        rows = [ele for ele in rows if ele != []]
        return pd.DataFrame(rows, columns=headers)

    def get_table_headers(self, table):
        headers = []
        for th in table.find("tr").find_all("th"):
            headers.append(th.text.strip())
        headers.pop(0)
        return headers

    def get_table_rows(self, table):
        output_rows = []
        for table_row in table.findAll('tr'):
            columns = table_row.findAll('td')
            output_row = []
            for column in columns:
                output_row.append(column.text)
            output_rows.append(output_row)
        return output_rows

    def writeFile(self, data, fileName, dataType):
        file = open(fileName, dataType)
        file.write(data)
        file.close()
