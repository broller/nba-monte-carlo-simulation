import os.path
from pathlib import Path
from bs4 import BeautifulSoup
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


class FDPropsScraper:
    def __init__(self):
        super().__init__()
    
    def writeProps(self, eventId):
        filename = str(Path(__file__).parent.absolute()) + '/temp/{event}.html'.format(event=eventId)
        if not os.path.exists(filename):
            url = "https://in.sportsbook.fanduel.com/basketball/nba/{event}?tab=player-points".format(event=eventId)
            options = webdriver.ChromeOptions()
            options.add_argument('headless')
            options.add_argument("--log-level=3")
            browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
            browser.get(url)
            html = browser.page_source
            browser.quit()
            self.writeFile(html, filename, 'w')
            soup = BeautifulSoup(html, features="html.parser")
        else:
            soup = BeautifulSoup(open(filename), features="html.parser")

        return self.buildMarketObject(soup.findAll("div", {"class": "t ac v w av aw y eb h dz da ea"}))


    def buildMarketObject(self, players):
        markets = {}
        for i, player in enumerate(players):
            name = player.find('span').text
            ou = player.findAll('div', {"role": "button"})
            markets[i] = {"playerName": name, "value": self.getValue(ou[0]), "overOdds": self.getOdds(ou[0]), "underOdds": self.getOdds(ou[1])}
        return markets

    def getValue(self, market):
        return market.find('span').text.split()[1]

    def getOdds(self, market):
        return market.findAll('span')[1].text

    def writeFile(self, data, fileName, dataType):
        file = open(fileName, dataType)
        file.write(data)
        file.close()


# scraper = FDPropsScraper()
# print(scraper.writeProps('brooklyn-nets-@-milwaukee-bucks-30966282'))