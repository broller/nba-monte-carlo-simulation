import requests
import os.path
from pathlib import Path
from bs4 import BeautifulSoup

class DKPropsScraper:
    def __init__(self):
        super().__init__()
    
    def writeProps(self, eventId):
        fileName = str(Path(__file__).parent.absolute()) + '/temp/' + eventId + '.html'
        if os.path.exists(fileName):
            soup = BeautifulSoup(open(fileName), features="html.parser")
            propsMarketTable = self.findMarket(soup.findAll("div", { "class": "sportsbook-event-accordion__wrapper"}))
            return self.buildMarketObject(propsMarketTable) if propsMarketTable is not None else None
        else:
            url = 'https://sportsbook.draftkings.com/event/{event}?category=odds&subcategory=player-props'.format(event=eventId)
            response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
            self.writeFile(response.content, fileName, 'wb')
            soup = BeautifulSoup(response.content, features="html.parser")
            propsMarketTable = self.findMarket(soup.findAll("div", { "class": "sportsbook-event-accordion__wrapper"}))
            return self.buildMarketObject(propsMarketTable) if propsMarketTable is not None else None

    def findMarket(self, propMarket):
        for market in propMarket:
            aTags = market.find('a')
            if aTags.text == 'Points':
                return market.find('table').find('tbody')
        
        return None
    
    def buildMarketObject(self, table):
        playerProps = table.findAll('tr')
        markets = {}
        for i, player in enumerate(playerProps):
            props = player.findAll('td')
            markets[i] = { "playerName": self.normalizeName(player.find('th').text), "value": self.getValue(props[0]), "overOdds": self.getOdds(props[0]), "underOdds": self.getOdds(props[1])} 
        return markets

    def normalizeName(self, name):
        return name.replace('Points', '').strip()
    
    def getOverUnder(self, market):
        return market.find("div", {"class": "sportsbook-outcome-body-wrapper"}).find("span", {"class": "sportsbook-outcome-cell__label"}).text
    
    def getValue(self, market):
        return market.find("span", {"class": "sportsbook-outcome-cell__line"}).text

    def getOdds(self, market):
        return market.find("span", {"class": "sportsbook-odds american default-color"}).text    

    def writeFile(self, data, fileName, dataType):
        file = open(fileName, dataType)
        file.write(data)
        file.close()