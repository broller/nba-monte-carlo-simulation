from dk_props_scraper import DKPropsScraper

class PropsScaper:
    def __init__(self):
        super().__init__()
        self.dkScraper = DKPropsScraper()
        self.options = {0 : self.dkScraper.writeProps}
    
    def getProps(self, sportsBookId, eventId):
        if sportsBookId < 4:
            return self.options[sportsBookId](eventId)
        else:
            return None

scraper = PropsScaper()
print(scraper.getProps(0, '180004418'))
