import json
import sys
from pathlib import Path

# todo: test this
# p = Path(__file__).parents[1]

# print(p)
# /absolute/path/to/two/levels/up
sys.path.append('/Users/brandonr/Software_Projects/wagering_tools/scrapers/stats-scrapers/common/')
sys.path.append('/Users/brandonr/Software_Projects/wagering_tools/scrapers/stats-scrapers/basketball/utils')
# sys.path.append('../../common')
sys.path.append('./')
sys.path.append(str(Path(__file__).parent.absolute() / 'props-scrapers'))
from affiliates import Affiliates
from dk_props_scraper import DKPropsScraper
from fd_props_scraper import FDPropsScraper
from pb_props_scraper import PBPropsScraper
from nba_team_util import NBATeamUtil
from roster_util import RosterUtil
from player_game_log_scraper import PlayerGameLogScraper
from prop_calculator import PropsCalculator

switcher = {Affiliates.DRAFTKINGS:  DKPropsScraper, Affiliates.FANDUEL: FDPropsScraper, Affiliates.POINTSBET: PBPropsScraper}
teamUtil = NBATeamUtil()
rosterUtil = RosterUtil()

if len(sys.argv) >= 2:
    scaper = switcher[Affiliates[sys.argv[1]]]()
    eventId = sys.argv[2]
    teamId1 = sys.argv[3]
    teamId2 = sys.argv[4]

    team1Roster = rosterUtil.getRoster(teamId1, 2022)
    team2Roster = rosterUtil.getRoster(teamId2, 2022)
    combinedRoster = team1Roster + team2Roster
    props = scaper.writeProps(eventId)
    playerGameLog = PlayerGameLogScraper()
    propsCalculator = PropsCalculator()
    calculatedProps = []
    for i in props:
        prop = props[i]
        player = rosterUtil.findPlayer(combinedRoster, prop['playerName'])
        if player is not None:
            gamelog = playerGameLog.getPlayerGameLog(player['id'], 2022)
            result = propsCalculator.calculatePropValue(prop, gamelog)
        # else:
        #     # print(prop['playerName'] + ' not found')
        #     prop['overEdge'] = 0
        #     prop['underEdge'] = 0
        calculatedProps.append(prop)
    
    print(json.dumps(calculatedProps, indent=4))
