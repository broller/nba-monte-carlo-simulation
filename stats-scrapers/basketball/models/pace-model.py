import sys
from pathlib import Path
# from turtle import home
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
# importing r2_score module
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
import numpy as np
from scipy.stats import norm
import random
import json
import pandas as pd

currentDirectory = Path(__file__).resolve()
path = currentDirectory.parents[1]
sys.path.append(str(path))
sys.path.append(str(path / 'league-stats-per-game'))
sys.path.append(str(path / 'league-summary'))
sys.path.append(str(path / 'utils'))

from league_pace_per_game_table import LeaguePacePerGameTable
from league_pace_summary_table import LeaguePaceSummaryTable
from nba_team_util import NBATeamUtil
from edge_calculator import EdgeCalculator

class Odds:
    def __init__(self, awayOdds, homeOdds) -> None:
        self.odds = (awayOdds, homeOdds)
    
    def awayOdds(self) -> int:
        return self.odds[0]
        
    def homeOdds(self) -> int:
        return self.odds[1]
    
    def overOdds(self) -> int:
        return self.odds[0]
    
    def underOdds(self) -> int:
        return self.odds[1]

class PaceModel:
    def __init__(self, perGameTable: LeaguePacePerGameTable, combinedHomeAway: bool) -> None:
        self.table = perGameTable
        self.combinedHomeAway = combinedHomeAway
        if combinedHomeAway:
            self.combinedModel = self.createCombinedModel(perGameTable)
        else:
            self.homeModel = self.createHomeModel(perGameTable)
            self.awayModel = self.createAwayModel(perGameTable)
        # # phoResult = model.intercept_ + (99.6 * model.coef_[0]) + (111.9 * model.coef_[1])

    def createCombinedModel(self, perGameTable) -> LinearRegression:
        homeStats = perGameTable.table[['homePace', 'homeORtg']].rename(columns={'homePace': 'teamPace', 'homeORtg': 'teamORtg'})
        awayStats = perGameTable.table[['awayPace', 'awayORtg']].rename(columns={'awayPace': 'teamPace', 'awayORtg': 'teamORtg'})
        homeScores =  perGameTable.table[['homeScore']].rename(columns={'homeScore': 'teamScore'})
        awayScores =  perGameTable.table[['awayScore']].rename(columns={'awayScore': 'teamScore'})

        x = pd.concat([homeStats, awayStats])[['teamPace', 'teamORtg']]
        y = pd.concat([homeScores, awayScores])['teamScore']

        x_train, self.homeX_test, y_train, self.homeY_test = train_test_split(x, y, test_size = 0.2, random_state = 42)
        model = LinearRegression()
        model.fit(x_train, y_train)
        return model

    def createHomeModel(self, perGameTable) -> LinearRegression:
        x = perGameTable.table[['homePace', 'homeORtg']]
        y = perGameTable.table['homeScore']
        x_train, self.homeX_test, y_train, self.homeY_test = train_test_split(x, y, test_size = 0.2, random_state = 42)
        model = LinearRegression()
        model.fit(x_train, y_train)
        return model

    def createAwayModel(self, perGameTable) -> LinearRegression:
        x = perGameTable.table[['awayPace', 'awayORtg']]
        y = perGameTable.table['awayScore']
        x_train, self.awayX_test, y_train, self.awayY_test = train_test_split(x, y, test_size = 0.2, random_state = 42)
        model = LinearRegression()
        model.fit(x_train, y_train)
        return model
    
    def getHomeModel(self) -> LinearRegression:
        if self.combinedHomeAway:
            return self.combinedModel
        else:
            return self.homeModel
    
    def getAwayModel(self) -> LinearRegression:
        if self.combinedHomeAway:
            return self.combinedModel
        else:
            return self.awayModel

    def getScore(self, model, pace, oRtg):
        # print(round(awayModel.intercept_ + (awayPaceDist * awayModel.coef_[0]) + (awayORtgDist * awayModel.coef_[1]), 0))
        return round(model.intercept_ + (pace * model.coef_[0]) + (oRtg * model.coef_[1]), 0)
    
    def testModel(self, model: LinearRegression, x_test, y_test) -> None:
        y_prediction =  model.predict(x_test)

        # # predicting the accuracy score
        score=r2_score(y_test,y_prediction)
        print('r2 score is ',score)
        print('mean_sqrd_error is ==',mean_squared_error(y_test,y_prediction))
        print('root_mean_squared error of is == ', np.sqrt(mean_squared_error(y_test,y_prediction)))
        print(model.intercept_)
        print(model.coef_)
        return
        
        
teamUtil = NBATeamUtil()
edgeCalculator = EdgeCalculator()
simulationRuns = 100000

def getPace(team, opp, summary):
    return ( float(team.iloc[0]['Pace']) * float(opp.iloc[0]['Pace']) ) / float(summary.getLeagueAverage()['Pace'])

def getORtg(team, opp, summary):
    return ( float(team.iloc[0]['ORtg'])  * float(opp.iloc[0]['DRtg']) ) / float(summary.getLeagueAverage()['ORtg'])

def getSpreadResult(awayScore, homeScore, homeSpread):
    if (homeScore + homeSpread) > awayScore:
        return 1
    else:
        return 0

def getTotalResult(awayScore, homeScore, total):
    if (homeScore + awayScore) > total:
        return 1
    else:
        return 0

def outputTotal(total, odds: Odds) -> None:
    print('The over  hit ' + "{:.2%}".format( ((total / simulationRuns))) + ' percent of the time')
    print('Over edge is ' + "{:.2%}".format(edgeCalculator.calculateEdge(odds.overOdds(), (total / simulationRuns))))
    print('The under hit ' + "{:.2%}".format((( (simulationRuns - total) / simulationRuns))) + ' percent of the time')
    print('Under edge is ' + "{:.2%}".format(edgeCalculator.calculateEdge(odds.underOdds(), (simulationRuns - total) / simulationRuns)))
    print()

def outputML(awayID, homeID, awayWins, homeWins, odds: Odds) -> None:
    print('ML:')
    print(teamUtil.getTeam(awayID) + ' won ' + "{:.2%}".format( ((awayWins / simulationRuns))) + ' percent of the time')
    print(teamUtil.getTeam(awayID) + ' edge is ' + "{:.2%}".format(edgeCalculator.calculateEdge(odds.awayOdds(), (awayWins / simulationRuns))))
    print(teamUtil.getTeam(homeID) + ' won ' + "{:.2%}".format( ((homeWins / simulationRuns))) + ' percent of the time')
    print(teamUtil.getTeam(homeID) + ' edge is ' + "{:.2%}".format(edgeCalculator.calculateEdge(odds.homeOdds(), (homeWins / simulationRuns))))
    print()

def outputSpread(awayID, homeID, spreadWins, odds) -> None:
    print('SPREAD:')
    print(teamUtil.getTeam(awayID) + ' beat the spread ' + "{:.2%}".format((( (simulationRuns - spreadWins) / simulationRuns))) + ' percent of the time')
    print(teamUtil.getTeam(awayID) + ' edge is ' + "{:.2%}".format(edgeCalculator.calculateEdge(odds[1], ( (simulationRuns - spreadWins) / simulationRuns))))
    print(teamUtil.getTeam(homeID) + ' beat the spread ' + "{:.2%}".format( ((spreadWins / simulationRuns))) + ' percent of the time')
    print(teamUtil.getTeam(homeID) + ' edge is ' + "{:.2%}".format(edgeCalculator.calculateEdge(odds[0], (spreadWins / simulationRuns))))
    print()

def formatPTSresults(overUnder, awayWins, homeWins, spreadWins, odds):
    obj = {}
    obj['homeMLPct'] = homeWins / simulationRuns
    obj['homeMLEdge'] = edgeCalculator.calculateEdge(odds['mlOdds'].homeOdds(), (homeWins / simulationRuns))
    obj['awayMLPct'] = awayWins / simulationRuns
    obj['awayMLEdge'] = edgeCalculator.calculateEdge(odds['mlOdds'].awayOdds(), (awayWins / simulationRuns))
    
    obj['overPct'] = overUnder / simulationRuns
    obj['overEdge'] = edgeCalculator.calculateEdge(odds['totalOdds'].overOdds(), (overUnder / simulationRuns))
    obj['underPct'] =  (simulationRuns - overUnder) / simulationRuns
    obj['underEdge'] = edgeCalculator.calculateEdge(odds['totalOdds'].underOdds(), (simulationRuns - overUnder) / simulationRuns)

    obj['homeSpreadPct'] =  spreadWins / simulationRuns
    obj['homeSpreadEdge'] = edgeCalculator.calculateEdge(odds['spreadOdds'].homeOdds(), (spreadWins / simulationRuns))
    obj['awaySpreadPct'] =  (simulationRuns - spreadWins) / simulationRuns
    obj['awaySpreadEdge'] = edgeCalculator.calculateEdge(odds['spreadOdds'].awayOdds(), ( (simulationRuns - spreadWins) / simulationRuns))
    print(json.dumps(obj, indent=4))

if len(sys.argv) >= 3 and teamUtil.getTeam(sys.argv[1]) and teamUtil.getTeam(sys.argv[2]):
    awayID = sys.argv[1]
    homeID = sys.argv[2]
    awayName = teamUtil.getTeam(awayID)
    homeName = teamUtil.getTeam(homeID)

    perGameTable = LeaguePacePerGameTable()
    summaryTable = LeaguePaceSummaryTable()
    paceModel = PaceModel(perGameTable, True)

    awayTeam = perGameTable.getTeamStatsByField(awayID, ['Pace', 'ORtg'])
    awayPace= getPace(summaryTable.getTeamPaceStats(awayName), summaryTable.getTeamPaceStats(homeName), summaryTable)
    awayORtg = getORtg(summaryTable.getTeamPaceStats(awayName), summaryTable.getTeamPaceStats(homeName), summaryTable)

    homeTeam = perGameTable.getTeamStatsByField(homeID, ['Pace', 'ORtg'])
    homePace= getPace(summaryTable.getTeamPaceStats(homeName), summaryTable.getTeamPaceStats(awayName), summaryTable)
    homeORtg = getORtg(summaryTable.getTeamPaceStats(homeName), summaryTable.getTeamPaceStats(awayName), summaryTable)

    awayWins = 0
    homeWins = 0
    ou = 0
    spreadWins = 0
    total = float(sys.argv[3])
    spread = float(sys.argv[4])
    odds = {'mlOdds': Odds(float(sys.argv[5]), float(sys.argv[6])), 'totalOdds': Odds(float(sys.argv[7]), float(sys.argv[8])), 'spreadOdds': Odds(float(sys.argv[9]), float(sys.argv[10]))}
    
    for i in range(simulationRuns):
        while True:
            # awayPaceDist = norm.ppf(random.uniform(0, 1), loc=awayPace, scale=awayTeam['Pace'].std())
            awayORtgDist = norm.ppf(random.uniform(0, 1), loc=awayORtg, scale=awayTeam['ORtg'].std())
            awayScore = paceModel.getScore(paceModel.getAwayModel(), awayPace, awayORtgDist)

            # homePaceDist = norm.ppf(random.uniform(0, 1), loc=homePace, scale=homeTeam['Pace'].std())
            homeORtgDist = norm.ppf(random.uniform(0, 1), loc=homeORtg, scale=homeTeam['ORtg'].std())
            homeScore = paceModel.getScore(paceModel.getHomeModel(), homePace, homeORtgDist)
           
            if homeScore != awayScore:
                break
        
        ou += getTotalResult(awayScore, homeScore, total)
        spreadWins += getSpreadResult(awayScore, homeScore, spread)

        if homeScore > awayScore:
            homeWins += 1
        elif awayScore > homeScore:
            awayWins +=1

    # outputTotal(ou, odds['totalOdds'])
    # outputML(awayID, homeID, awayWins, homeWins, odds['mlOdds'])
    # outputSpread(awayID, homeID, spreadWins, odds['spreadOdds'])
    formatPTSresults(ou, awayWins, homeWins, spreadWins, odds)
else:
    print('Not enough arguments passed')