import pandas as pd
import math
import sys
from pathlib import Path
import numpy 
import scipy.optimize
from scipy.stats import norm
import statsmodels.api as sm
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics
from scipy.optimize import minimize
import scipy.stats as stats

currentDirectory = Path(__file__).resolve()
path = currentDirectory.parents[1]
sys.path.append(str(path))
sys.path.append(str(path / 'league-stats-per-game'))
sys.path.append(str(path / 'utils'))

from league_stats_per_game_table import LeaguePerGameStatsTable
from nba_team_util import NBATeamUtil
from edge_calculator import EdgeCalculator

def buildInitialTable(leagueTable, teamTable):
    btModel = leagueTable.table[['awayTeam', 'awayPTS', 'homeTeam', 'homePTS']].copy()
    btModel.loc[:,'gameTotal'] = btModel.apply(lambda row: row.homePTS + row.awayPTS, axis=1)
    btModel.loc[:,'homeMOV'] = btModel.apply(lambda row: row.homePTS - row.awayPTS, axis=1)
    btModel.loc[:,'gameResult'] = btModel.apply(lambda row: 1 if row.homePTS > row.awayPTS else 0 , axis=1)
    # btModel['logisticFunction'] = btModel.apply(lambda row: logisticFunction(0, teamTable.loc[teamTable['Team'] == row.homeTeam]['logisticStrength'].values[0], teamTable.loc[ teamTable['Team'] == row.awayTeam]['logisticStrength'].values[0]), axis=1)
    # btModel['resultFunction'] = btModel.apply(lambda row: row.logisticFunction if row.gameResult == 1 else 1 - row.logisticFunction, axis=1)
    return btModel

def setLogisticFunction(btModel, teamTable, hfa):
    btModel.loc[:,'logisticFunction'] = btModel.apply(lambda row: logisticFunction(hfa, teamTable.loc[teamTable['Team'] == row.homeTeam]['logisticStrength'].values[0], teamTable.loc[ teamTable['Team'] == row.awayTeam]['logisticStrength'].values[0]), axis=1)
    return btModel

def setResultFunction(btModel):
    btModel.loc[:,'resultFunction'] = btModel.apply(lambda row: row.logisticFunction if row.gameResult == 1 else 1 - row.logisticFunction, axis=1)
    return btModel

def buildInitialTeamTable():
    teamUtil = NBATeamUtil()
    table = pd.DataFrame(teamUtil.getKeys(), columns = ['Team'])
    table['numericId'] = table.index + 1
    table.loc[:,'logisticStrength'] = table.apply(lambda row: 1 , axis=1)
    table['rank'] = table['logisticStrength'].rank(method = 'max')
    return table

def logisticFunction(hfa, hta, ata):
    # print( 1 / (1 + math.exp( -(0.362 + 2.756 - -1.456))))
    return 1 / (1 + math.exp( -(hfa + hta - ata) ))

def errorfn(w,m,s):
    return w.dot(m) - s

def determineHomeFieldAdvantage(gameTable, teamTable):
    games = len(gameTable.index)
    teamNums = len(teamTable.index)

    # First we create a matrix M which will hold the data on
    # who played whom in each game and who had home-field advantage.
    m_rows = teamNums + 1
    m_cols = games
    M = numpy.zeros( (m_rows, m_cols) )

    # Then we create a vector S which will hold the final
    # relative scores for each game.
    s_cols = games
    S = numpy.zeros(s_cols)
    
    game = outcomeTable[['homeTeam', 'awayTeam', 'homePTS', 'awayPTS']].to_numpy()
    # Loading M and S with game data
    for col,gamedata in enumerate(game):
        home,away,homescore,awayscore = gamedata
        # In the csv data, teams are numbered starting at 1
        # So we let home-team advantage be 'team 0' in our matrix
        M[0, col] =  1.0   # home team advantage
        M[int(teamTable.loc[teamTable['Team'] == home]['numericId']), col] =  1.0
        M[int(teamTable.loc[teamTable['Team'] == away]['numericId']), col] = -1.0
        S[col] = int(homescore) - int(awayscore)

    init_W = numpy.array([2.0]+[0.0] * teamNums) 
    W = scipy.optimize.leastsq(errorfn, init_W, args=(M,S))
    homeAdvantage = W[0][0]   # 2.2460937500005356
    teamStrength = W[0][1:]   # numpy.array([-151.31111318, -136.36319652, ... ])
    teamStrength -= teamStrength.mean()
    return homeAdvantage, teamStrength

def runLinearRegression():
    X = outcomeTable['logisticFunction']
    Y = outcomeTable['homeMOV']
    X = sm.add_constant(X)
    results = sm.OLS(Y, X).fit()
    # print(results.summary())
    stdErr = results.resid.std(ddof=X.shape[1])
    regIntercept = results.params[0]
    logFuncCoef = results.params[1]
    return stdErr, regIntercept, logFuncCoef

def testLinearRegression():
    X = outcomeTable.logisticFunction.values
    Y = outcomeTable.homeMOV.values
    X = X.reshape(X.size, 1)
    Y = Y.reshape(Y.size, 1)
   
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=0)
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)
    y_pred = regressor.predict(X_test)
    df = pd.DataFrame({'Actual': y_test.reshape(1, y_test.size)[0], 'Predicted': y_pred.reshape(1, y_pred.size)[0]})
    print(df)
    print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
    print('Root Mean Squared Error:', numpy.sqrt(metrics.mean_squared_error(y_test, y_pred)))
    return

def getTeamStrength(key):
    return teamTable.loc[teamTable['Team'] == key]['logisticStrength'].values[0]

# Examples args
# MIA @ DET with MIAMI favored by 9 with home and away odds both at -110
# DET MIA -9 -110 -110
if len(sys.argv) >= 5:
    # build the team ranking table and game tables
    teamTable = buildInitialTeamTable()
    outcomeTable = buildInitialTable(LeaguePerGameStatsTable(), teamTable)
    outcomeTable.to_csv('bradley-terry.csv', sep=',')

    # determine HFA and rank teams in team table
    homeAdvantage, teamStrength = determineHomeFieldAdvantage(outcomeTable, teamTable)
    teamTable.loc[:,'logisticStrength'] = teamStrength
    teamTable['rank'] = teamTable['logisticStrength'].rank(method = 'max', ascending=0)
    # # now set log function and result function on each game
    setLogisticFunction(outcomeTable, teamTable, homeAdvantage)
    setResultFunction(outcomeTable)
    print(homeAdvantage)

    product = outcomeTable['resultFunction'].prod()
    print(product)
    print(outcomeTable)
    logLikelihood = math.log(product)

    # # gets standard error and coefficients and run model
    stdErr, regIntercept, logFuncCoef = runLinearRegression()
    # testLinearRegression()
    homeTeam = getTeamStrength(sys.argv[1])
    awayTeam = getTeamStrength(sys.argv[2])
    homeLogFunc = logisticFunction(homeAdvantage, homeTeam, awayTeam) # home: 1/(1+EXP(-(($Y$5)+VLOOKUP(AB6,$U$5:$V$22,2,FALSE)-VLOOKUP(AB5,$U$5:$V$22,2,FALSE)))
    awayLogFunc = 1 - logisticFunction(homeAdvantage, homeTeam, awayTeam) # away: 1-(1/(1+EXP(-(($Y$5)+VLOOKUP(AB6,$U$5:$V$22,2,FALSE)-VLOOKUP(AB5,$U$5:$V$22,2,FALSE)))))

    estSpread = regIntercept + (logFuncCoef * homeLogFunc)
    actualSpread = float(sys.argv[3])

    homeNormDist = 1 - norm.cdf(actualSpread, estSpread, stdErr) # home norm dist: =1-NORMDIST(AE6,AD6,$Z$5,TRUE) 1 - norm.cdf(15, 32.39, 31.61857515)
    awayNormDist = 1 - norm.cdf(-actualSpread, -estSpread, stdErr)

    homeFairOdds = 1 / homeNormDist
    awayFairOdds = 1 / awayNormDist

    ec = EdgeCalculator()
    actualOdds = ec.americanToDecimal(float(sys.argv[4]))
    actualAwayOdds = ec.americanToDecimal(float(sys.argv[5]))

    homeEV = actualOdds * homeNormDist - 1
    awayEV = actualAwayOdds * awayNormDist - 1

    print('Estimated spread is ' + str(estSpread) + ' PTS')
    print('Actual spread is ' + str(actualSpread))
    print("Home Win Percent is {:.0%}".format(homeNormDist))
    print("Away Win Percent is {:.0%}".format(awayNormDist))
    print("Home EV is {:.0%}".format(homeEV))
    print("Away EV is {:.0%}".format(awayEV))

    X = outcomeTable['logisticFunction']
    Y = outcomeTable['homeMOV']
    # define likelihood function
    def MLERegression(params):
        intercept, beta, sd = params[0], params[1], params[2] # inputs are guesses at our parameters
        yhat = intercept + beta*X # predictions
        # next, we flip the Bayesian question
        # compute PDF of observed values normally distributed around mean (yhat)
        # with a standard deviation of sd
        negLL = -numpy.sum( stats.norm.logpdf(Y, loc=yhat, scale=sd) )
        # return negative LL
        return(negLL)

    # let’s start with some random coefficient guesses and optimize
    guess = numpy.array([1,1,1])
    results = minimize(MLERegression, guess, method = 'Nelder-Mead', options={'disp': True})
    print(results)