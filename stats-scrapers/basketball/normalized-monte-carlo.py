import json
import pandas as pd
from scipy.stats import norm
import random
from pathlib import Path
import sys
sys.path.append(str(Path(__file__).parent.absolute() / 'league-stats-per-game'))
sys.path.append(str(Path(__file__).parent.absolute() / 'league-summary'))
sys.path.append(str(Path(__file__).parent.absolute() / 'utils'))
from edge_calculator import EdgeCalculator
from league_stats_per_game_table import LeaguePerGameStatsTable
from league_summary_table import LeagueSummaryTable
from nba_team_util import NBATeamUtil

teamUtil = NBATeamUtil()
edgeCalculator = EdgeCalculator()

simulationRuns = 100000

def getTeam3PMonteCarloObject(leagueAvgTable, team1, team2):
    headers = ['3P', '3PA', '3P%', 'SD']
    matchupTable = pd.DataFrame(columns=headers)

    team1ThreesMade = (team1.iloc[0]['3PAAvg'] * team2.iloc[0]['o3PAAvg']) / leagueAvgTable.iloc[0]['3PA']
    team2ThreesMade = (team2.iloc[0]['3PAAvg'] * team1.iloc[0]['o3PAAvg']) / leagueAvgTable.iloc[0]['3PA']
    matchupTable['3PA'] = [team1ThreesMade, team2ThreesMade]

    team1ThreePercent = (team1.iloc[0]['3P%'] * team2.iloc[0]['o3P%']) / leagueAvgTable.iloc[0]['3P%']
    team2ThreePercent = (team2.iloc[0]['3P%'] * team1.iloc[0]['o3P%']) / leagueAvgTable.iloc[0]['3P%']
    matchupTable['3P%'] = [team1ThreePercent, team2ThreePercent]

    matchupTable['3P'] = [(team1ThreesMade * team1ThreePercent), (team2ThreesMade * team2ThreePercent)]

    teams1PerGame = perGameTable.getTeamStatsByField(team1.iloc[0]['id'], ['3PA', '3P'])
    teams2PerGame = perGameTable.getTeamStatsByField(team2.iloc[0]['id'], ['3PA', '3P'])
    matchupTable['SD'] = [teams1PerGame['3P'].std(), teams2PerGame['3P'].std()]

    return matchupTable

def getTeamPTSMCObject(leagueAvgTable, team1, team2):
    headers = ['PTS', 'SD']
    matchupTable = pd.DataFrame(columns=headers)

    team1ThreesMade = (team1.iloc[0]['PTSAvg'] * team2.iloc[0]['oPTSAvg']) / leagueAvgTable.iloc[0]['PTS']
    team2ThreesMade = (team2.iloc[0]['PTSAvg'] * team1.iloc[0]['oPTSAvg']) / leagueAvgTable.iloc[0]['PTS']
    matchupTable['PTS'] = [team1ThreesMade, team2ThreesMade]

    teams1PerGame = perGameTable.getTeamStatsByField(team1.iloc[0]['id'], ['PTS'])
    teams2PerGame = perGameTable.getTeamStatsByField(team2.iloc[0]['id'], ['PTS'])
    matchupTable['SD'] = [teams1PerGame['PTS'].std(), teams2PerGame['PTS'].std()]

    return matchupTable

def outputPTSresults(overUnder, teamID, team2ID, wins, odds):
    print('MONEYLINE:')
    print(teamUtil.getTeam(teamID1) + ' win ' + str(( (team1Wins / simulationRuns) * 100) ) + ' percent of the time')
    print(teamUtil.getTeam(teamID1) + ' edge is ' + str(edgeCalculator.calculateEdge(odds[0][0], (team1Wins / simulationRuns))))
    print(teamUtil.getTeam(teamID2) + ' win ' + str(( (team2Wins / simulationRuns) * 100) ) + ' percent of the time')
    print(teamUtil.getTeam(teamID2) + ' edge is ' + str(edgeCalculator.calculateEdge(odds[0][1], (team2Wins / simulationRuns))))
    print('-------------------------------')
    print('TOTAL:')
    print('The over  hit ' + str( ((overUnder / simulationRuns) * 100)) + ' percent of the time')
    print('Over edge is ' + str(edgeCalculator.calculateEdge(odds[1][0], (overUnder / simulationRuns))))
    print('The under hit ' + str( (( (simulationRuns - overUnder) / simulationRuns) * 100)) + ' percent of the time')
    print('Under edge is ' + str(edgeCalculator.calculateEdge(odds[1][1], (simulationRuns - overUnder) / simulationRuns)))
    print('-------------------------------')
    print('SPREAD:')
    print(teamUtil.getTeam(teamID) + ' beat the spread ' + str( ((wins / simulationRuns) * 100)) + ' percent of the time')
    print(teamUtil.getTeam(teamID1) + ' edge is ' + str(edgeCalculator.calculateEdge(odds[2][0], (wins / simulationRuns))))
    print(teamUtil.getTeam(team2ID) + ' beat the spread ' + str( (( (simulationRuns - wins) / simulationRuns) * 100)) + ' percent of the time')
    print(teamUtil.getTeam(teamID2) + ' edge is ' + str(edgeCalculator.calculateEdge(odds[2][1], ( (simulationRuns - wins) / simulationRuns))))
    print('-------------------------------')

def formatPTSresults(overUnder, teamID, team2ID, wins, odds):
    obj = {}
    obj['favoriteMLPct'] = team1Wins / simulationRuns
    obj['favoriteMLEdge'] = edgeCalculator.calculateEdge(odds[0][0], (team1Wins / simulationRuns))
    obj['underdogMLPct'] = team2Wins / simulationRuns
    obj['underdogMLEdge'] = edgeCalculator.calculateEdge(odds[0][1], (team2Wins / simulationRuns))
    
    obj['overPct'] = overUnder / simulationRuns
    obj['overEdge'] = edgeCalculator.calculateEdge(odds[1][0], (overUnder / simulationRuns))
    obj['underPct'] =  (simulationRuns - overUnder) / simulationRuns
    obj['underEdge'] = edgeCalculator.calculateEdge(odds[1][1], (simulationRuns - overUnder) / simulationRuns)

    obj['favoriteSpreadPct'] =  wins / simulationRuns
    obj['favoriteSpreadEdge'] = edgeCalculator.calculateEdge(odds[2][0], (wins / simulationRuns))
    obj['underdogSpreadPct'] =  (simulationRuns - wins) / simulationRuns
    obj['underdogSpreadEdge'] = edgeCalculator.calculateEdge(odds[2][1], ( (simulationRuns - wins) / simulationRuns))
    print(json.dumps(obj, indent=4))
    

if len(sys.argv) >= 4 and teamUtil.getTeam(sys.argv[1]) and teamUtil.getTeam(sys.argv[2]):
    teamID1 = sys.argv[1]
    teamID2 = sys.argv[2]

    perGameTable = LeaguePerGameStatsTable()
    leagueSummaryTable = LeagueSummaryTable()
    
    if sys.argv[3] == '3P':
        team1 = leagueSummaryTable.getTeam(teamID1, ['3P', '3PA', '3P%','G'])
        leagueSummaryTable.addAvg(team1, ['3P', '3PA'])

        team2 = leagueSummaryTable.getTeam(teamID2, ['3P', '3PA', '3P%','G'])
        leagueSummaryTable.addAvg(team2, ['3P', '3PA'])
        leagueAvgs = leagueSummaryTable.getLeagueAvg(['3P', '3PA', '3P%'])    
        normalizedMCObject = getTeam3PMonteCarloObject(leagueAvgs, team1, team2)
    elif sys.argv[3] == 'PTS':
        leagueAvgs = leagueSummaryTable.getLeagueAvg(['PTS'])
    
        team1 = leagueSummaryTable.getTeam(teamID1, ['PTS','G'])
        leagueSummaryTable.addAvg(team1, ['PTS'])

        team2 = leagueSummaryTable.getTeam(teamID2, ['PTS','G'])
        leagueSummaryTable.addAvg(team2, ['PTS'])

        normalizedMCObject = getTeamPTSMCObject(leagueAvgs, team1, team2)
    else:
        print('Error: no sim to run.')


    team1Wins = 0
    team2Wins = 0
    ou = 0
    spreadWins = 0
    total = float(sys.argv[4])
    spread = float(sys.argv[5])

    odds = [[int(sys.argv[6]), int(sys.argv[7])], [int(sys.argv[8]), int(sys.argv[9])], [int(sys.argv[10]), int(sys.argv[11])]]

    for number in range(0, simulationRuns):
        team1Dist = round(norm.ppf(random.uniform(0, 1), loc=normalizedMCObject.iloc[0][sys.argv[3]], scale=normalizedMCObject.iloc[0]['SD']))
        team2Dist = round(norm.ppf(random.uniform(0, 1), loc=normalizedMCObject.iloc[1][sys.argv[3]], scale=normalizedMCObject.iloc[1]['SD']))

        if sys.argv[3] == 'PTS':
            if (team1Dist + team2Dist) > total:
                ou += 1

            if (team1Dist + spread) > team2Dist:
                spreadWins += 1

        if team1Dist > team2Dist:
            team1Wins += 1
        else:
            team2Wins +=1

    # print(teamUtil.getTeam(teamID1) + ' win ' + str(( (team1Wins / simulationRuns) * 100) ) + ' percent of the time')
    # print(teamUtil.getTeam(teamID2) + ' win ' + str(( (team2Wins / simulationRuns) * 100) ) + ' percent of the time')
    if sys.argv[3] == 'PTS':
        formatPTSresults(ou, teamID1, teamID2, spreadWins, odds)
        # outputPTSresults(ou, teamID1, teamID2, spreadWins, odds)
else:
    print('Error not enough arguments passed')