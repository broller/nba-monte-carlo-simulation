from enum import Enum

class Affiliates(Enum):
    DRAFTKINGS = 'DRAFTKINGS'
    FANDUEL = 'FANDUEL'
    POINTSBET = 'POINTSBET'