from bs4 import BeautifulSoup
import pandas as pd
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

def writeFile(data, fileName, dataType):
    file = open(fileName, dataType)
    file.write(data)
    file.close()

def getTeamStatsHTML(team, year):
    url = "https://www.basketball-reference.com/teams/{team}/{year}.html".format(team=team,year=year)

    # for reading dynamic content, but having to wait longer
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    browser = webdriver.Chrome('../chromedriver')
    browser.get(url)
    timeout = 30
    try:
        element_present = EC.presence_of_element_located((By.XPATH, '//*[@id="team_and_opponent"]'))
        WebDriverWait(browser, timeout).until(element_present)
    except TimeoutException:
        print("Timed out waiting for page to load")
    html = browser.page_source

    writeFile(html, "{}_stats.html".format(team),"w")

    browser.quit()

def getTeamStatsTable(html):
    soup = BeautifulSoup(html, features="html.parser")
    table = soup.find("table", {"id": "team_and_opponent"})
    headers = [th.getText() for th in table.findAll('tr', limit=2)[0].findAll('th')]
    headers = headers[1:]

    rows = table.findAll('tr')[1:]
    team_stats = [[td.getText() for td in rows[i].findAll('td')] for i in range(len(rows))]

    stats = pd.DataFrame(team_stats, columns = headers)
    return stats


# first do getTeamStatsHTML("PHI", 2021)
team1Table = getTeamStatsTable(open("./PHI_stats.html"))
print(team1Table)

# first do getTeamStatsHTML("TOR", 2021)
team2Table = getTeamStatsTable(open("./TOR_stats.html"))
print(team2Table)


# html = getTeamStatsHTML("PHI", 2021)
# soup = BeautifulSoup(open("./PHI_stats.html"), features="html.parser")