import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import json
import re

def isSpread(element):
    hasPointsAndOdds = element.find("span", {"class": "fsu5r7i"}) and element.find("span", {"class": "fheif50"})
    return hasPointsAndOdds and not containsOverUnder(element.find("span", {"class": "fsu5r7i"}).text)

def getSpread(element):
    # fheif50=odds, fsu5r7i=spread 
    points = element.find("span", {"class": "fsu5r7i"}).text
    odds = element.find("span", {"class": "fheif50"}).text
    return {"points": points, "odds": odds}

def isMoneyline(element):
    return not element.find("span", {"class": "fsu5r7i"}) and element.find("span", {"class": "fheif50"})

def getMoneyline(element):
    # fheif50=odds
    odds = element.find("span", {"class": "fheif50"}).text
    return odds

def isTotal(element):
    # odds=fsu5r7i
    hasPointsAndOdds = element.find("span", {"class": "fsu5r7i"}) and element.find("span", {"class": "fheif50"})
    return hasPointsAndOdds and containsOverUnder(element.find("span", {"class": "fsu5r7i"}).text)

def containsOverUnder(str):
    return str.find('Ov') != -1 or str.find('Un') != -1

def getTotal(element):
    totalText = element.find("span", {"class": "fsu5r7i"}).text
    split = re.split("(Ov|Un)", totalText)
    if split[1] == "Ov":
        overUnder = "O"
    else:
        overUnder = "U"

    total = split[2].strip()
    odds = element.find("span", {"class": "fheif50"}).text
    return {"overUnder": overUnder, "total": total, "odds": odds}

def split_list(a_list):
    half = len(a_list)//2
    return a_list[:half], a_list[half:]

# url = 'https://in.pointsbet.com/sports/basketball/NBA'
# response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
# html = response.content

soup = BeautifulSoup(open("/Users/brandonr/Software_Projects/wagering_tools/scrapers/pb_output_nba.html"), features="html.parser")
events = soup.findAll("div", {"class": "f1oyvxkl"}) #f1oyvxkl = div containing game 
matchups = {}
for i in range(len(events)):
    id = i
    teams = events[i].findAll("div", {"class":"f2rhni5"}) # f2rhni5 = team name
    if teams:
        eventId = events[i].find('a')['href'].replace('/sports/basketball/NBA/', '')
        awayTeam = teams[0].find("p").text
        homeTeam = teams[1].find("p").text
        buttons = events[i].findAll("button", {"class" : "fi4vfzh f14nmd6v"})
        awayButtons, homeButtons = split_list(buttons)
        awaySpreadObj = ""
        homeSpreadObj = ""
        awayMoneyline = ""
        homeMoneyline = ""
        awayTotalObj = ""
        homeTotalObj = ""
        for k in range(len(awayButtons)):
            if isSpread(awayButtons[k]):
                awaySpreadObj = getSpread(awayButtons[k])
                homeSpreadObj = getSpread(homeButtons[k])
            elif isMoneyline(awayButtons[k]):
                awayMoneyline = getMoneyline(awayButtons[k])
                homeMoneyline = getMoneyline(homeButtons[k])
            elif isTotal(awayButtons[k]):
                awayTotalObj = getTotal(awayButtons[k])
                homeTotalObj = getTotal(homeButtons[k])

        # arr = {'team': team, 'spread': spread, 'total': overUnder, 'moneyline': moneyline}
        side1 = {'eventId': eventId, 'team': awayTeam, 'spread': awaySpreadObj, 'total':  awayTotalObj, 'moneyline': awayMoneyline}
        side2 = {'eventId': eventId, 'team': homeTeam, 'spread': homeSpreadObj, 'total': homeTotalObj, 'moneyline': homeMoneyline}

        matchups[id] = [side1, side2]

print(json.dumps(matchups, indent=4))