import csv
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import json

# url = 'https://in.sportsbook.fanduel.com/sports/navigation/830.1/10107.3'
# options = webdriver.ChromeOptions()
# options.add_argument('headless')
# browser = webdriver.Chrome('./chromedriver')
# browser.get(url)
# html = browser.page_source
# browser.quit()

def findTeams(element):
    aTag = element.find("a")["title"]
    teams = [x.strip() for x in aTag.split("@")]
    return teams

def getBetButtons(element):
    betButtons = element.findAll("div", {"role": "button"})
    length = len(betButtons)
    middle_index = length//2
    first_half = betButtons[:middle_index]
    second_half = betButtons[middle_index:]
    return [first_half, second_half]

def getBetValues(betOptions):
    betValues = {}
    for option in betOptions:
        values = option.findAll("span")
        if len(values) == 1:
            betValues.update(getMoneyline(values[0]))
        elif len(values) > 1:
            if isTotal(values[0]):
                betValues.update(getTotal(values))
            else:
                betValues.update(getSpread(values))
        else:
            raise ValueError('An option does not match set bet types')

    return betValues

def getEventId(el):
    return el.find('a')['href'].replace('/basketball/nba/', '')

def getMoneyline(el):
    return { 'moneyline': el.text }

def getSpread(els):
    return { "spread": {"points": els[0].text, "odds": els[1].text} }

def getTotal(els):
    total = [x.strip() for x in els[0].text.split()] 
    return { "total": {"overUnder": total[0], "total": total[1] ,"odds": els[1].text} }

def isTotal(el):
    return 'O' in el.text or 'U' in el.text

soup = BeautifulSoup(open("/Users/brandonr/Software_Projects/wagering_tools/scrapers/fd_output_nba.html"), features="html.parser")
tables = soup.findAll("div", {"class": "t u v w av aw y fj cs h db ev fm bn"})
matchups = {}
for i in range(len(tables)):
    id = i
    eventId = getEventId(tables[i])
    teams = findTeams(tables[i])
    awayTeam = {'eventId': eventId, 'team': teams[0] }
    homeTeam = {'eventId': eventId, 'team': teams[1] }
    betOptions = getBetButtons(tables[i])
    awayOptions = getBetValues(betOptions[0])
    homeOptions = getBetValues(betOptions[1])

    awayTeam.update(awayOptions)
    homeTeam.update(homeOptions)

    matchups[id] = [awayTeam, homeTeam]

print(json.dumps(matchups, indent=4))