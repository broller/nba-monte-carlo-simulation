import csv
import requests
from bs4 import BeautifulSoup

# url = 'https://sportsbook.draftkings.com/leagues/basketball/3230960?category=game-lines&subcategory=game'
# response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
# html = response.content
def getId(row):
    teamHeader = row.findAll("th", {"class": "sportsbook-table__column-row"})
    id = teamHeader[0].find("a", {"class": "event-cell-link"})["href"].split("/")[2]
    return id

def getTeamName(row):
    teamHeader = row.findAll("th", {"class": "sportsbook-table__column-row"})
    return teamHeader[0].find("span", {"class": "event-cell__name"}).text

def getMarket(row, index):
    markets = row.findAll("td", {"class": "sportsbook-table__column-row"})
    return markets[index]

def getSpread(row):
    spreadMarket = getMarket(row, 0)
    spread = { "points": spreadMarket.find("span", {"class": "sportsbook-outcome-cell__line"}).text, "odds": spreadMarket.find("span", {"class": "sportsbook-odds"}).text}
    if(spread['points'] == 'pk'):
        spread['points'] = 0

    return spread

def getTotals(row):
    totalMarket = getMarket(row, 1)
    overUnder = ""
    if totalMarket.find("span", {"class": "sportsbook-outcome-cell__label"}):
        overUnder = totalMarket.find("span", {"class": "sportsbook-outcome-cell__label"}).text
        total = totalMarket.find("span", {"class": "sportsbook-outcome-cell__line"}).text
        overUnderOdds = totalMarket.find("span", {"class": "sportsbook-odds"}).text
        overUnder = {"overUnder": overUnder, "total": total, "odds": overUnderOdds}
    return overUnder

def getMoneyline(row):
    mlMarket = getMarket(row, 2)
    moneyline = ""
    if mlMarket.find("span", {"class": "sportsbook-odds"}):
        moneyline = mlMarket.find("span", {"class": "sportsbook-odds"}).text
    return moneyline

def getEventId(row):
    return row.find('a')['href']

def getTeamLine(row):
    # {'eventId': eventId, 'team': team, 'spread': spread, 'total': overUnder, 'moneyline': moneyline}
    eventId = getEventId(row).replace('/event/', '')
    team = getTeamName(row)
    spread = getSpread(row)
    total = getTotals(row)
    moneyline = getMoneyline(row)
    return {'eventId': eventId, 'team': team, 'spread': spread, 'total': total, 'moneyline': moneyline}

soup = BeautifulSoup(open("/Users/brandonr/Software_Projects/wagering_tools/scrapers/output_nba.html"), features="html.parser")
tables = soup.findAll("tbody", {"class": "sportsbook-table__body"})
matchups = {}
for table in tables:
    rows = table.findAll("tr", recursive=False)
    for row in rows:
        marketObject = getTeamLine(row)
        id = getId(row)

        if id in matchups:
            matchups[id].append(marketObject)
        else:
            matchups[id] = [marketObject]

print(matchups)
# w = csv.writer(open("output.csv", "w"))
# for key, val in matchups.items():
#     w.writerow([key, val])

# html writer
# file = open("output1.html","wb")
# file.write(html)
# file.close()