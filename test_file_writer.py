import sys
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

def writeFile(data, fileName, dataType):
    file = open(fileName, dataType)
    file.write(data)
    file.close()

def writeDKTestFile():
    ## for reading non-dynamic content
    url = 'https://sportsbook.draftkings.com/leagues/basketball/103?category=game-lines&subcategory=game'
    response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    html = response.content

    # html writer
    writeFile(html, "output_nba.html","wb")


def writeFDTestFile():
    # for reading dynamic content
    url = 'https://in.sportsbook.fanduel.com/navigation/nba'
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    browser.get(url)
    html = browser.page_source

    writeFile(html, "fd_output_nba.html", "w")

    browser.quit()

def writePBTestFile():
    # for reading dynamic content, but having to wait longer
    url = 'https://in.pointsbet.com/sports/basketball/NBA'
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    browser.get(url)
    timeout = 100000
    # /html/body/div[2]/div/div/div[1]/div/div/div[3]/div[1]/div/div/div[2]/div/div
    try:
        element_present = EC.presence_of_element_located((By.CLASS_NAME, 'f1oyvxkl')) 
        WebDriverWait(browser, timeout).until(element_present)
        browser.implicitly_wait(5)
    except TimeoutException:
        print("Timed out waiting for page to load")
    html = browser.page_source

    writeFile(html, "pb_output_nba.html","w")

    browser.quit()


affiliate = sys.argv[1]
options = {'0' : writeDKTestFile, '1' : writeFDTestFile, '2': writePBTestFile}
options[affiliate]()